export LANG='ja_JP.UTF-8'
export EDITOR='vim'
export PAGER='less'
export WORDCHARS='*?[]~\!#%^(){}<>|`#%^*()+?'
export LSCOLORS=gxfxxxxxcxxxxxxxxxxxxx
