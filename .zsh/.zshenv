platform=`uname`
if [[ "$platform" == 'Linux' ]]; then
    ## alias for linux
    alias ls='ls -F --color=auto'
    alias l.='ls -dF .* --color=auto'
    alias ll='ls -lF --color=auto'
else
    ## alias for bsd(osx)
    alias ls='ls -FG'
    alias l.='ls -dFG .*'
    alias ll='ls -lFG'
fi

## common alias
alias vi='vim'


umask 0002
