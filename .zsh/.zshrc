# Created by newuser for 4.3.10
bindkey -e

# zsh option
autoload -U compinit
compinit -u
zstyle ':completion:*' list-colors 'di=36' 'ln=35'
zstyle ':completion:*:default' menu select=1
setopt hist_ignore_dups
setopt share_history
setopt no_check_jobs
setopt no_hup
setopt magic_equal_subst
setopt ignore_eof
setopt extended_glob

# Shell options
HISTFILE=${ZDOTDIR}/.zsh_history
HISTSIZE=10000
SAVEHIST=10000

# prompt
if test -f ${ZDOTDIR}/.zshcolors
then
     source ${ZDOTDIR}/.zshcolors
     PROMPT="${COLOR_FG_FFFF00}[%~]
${COLOR_FG_00FFFF}%# ${STYLE_DEFAULT}"
     PROMPT2="${COLOR_FG_00FFFF}%_%%${STYLE_DEFAULT} "
     SPROMPT="${COLOR_FG_FF0000}%r is correct? [n,y,a,e]:${STYLE_DEFAULT} "
     [ -n "${REMOTEHOST}${SSH_CONNECTION}" ] &&
          PROMPT="${COLOR_FG_00FF00}${HOST%%.*} ${PROMPT}"
fi

# terminal title-
case "${TERM}" in
     kterm*|xterm)
          precmd() {
               echo -ne "\033]0;${USER}@${HOST%%.*}:${PWD}\007"
          }
          ;;
esac
